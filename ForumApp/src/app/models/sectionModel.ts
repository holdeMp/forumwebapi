export class SectionModel{
    constructor(public id?: number, 
                public name?: string,
                public subSectionsIds?:number[])
    {
      
    }
  }