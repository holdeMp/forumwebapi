﻿using DAL.Interfaces;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Interfaces
{
    public interface ISectionTitleRepository : IRepository<SectionTitle>
    {

    }
}
