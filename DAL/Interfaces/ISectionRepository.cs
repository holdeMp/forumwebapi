﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces
{
    public interface ISectionRepository : IRepository<Section>
    {

    }
}
